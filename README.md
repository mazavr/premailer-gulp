## Requirements
- Node.js
- Google Chrome **LiveReload** extension

## Preparation
- run cmd
- go to project folder
- install gulp globally
```sh
$ npm install --global gulp
```
- install npm modules
```sh
$ npm install
```

## Usage
- run cmd
- go to project folder
- run gulp
```sh
$ gulp
```
- go to [localhost:8080](http://localhost:8080)

