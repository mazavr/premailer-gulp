var gulp = require('gulp'),
    inlineCss = require('gulp-inline-css'),
    runSequence = require('run-sequence'),
    livereload = require('gulp-livereload'),
    http = require('http'),
    st = require('st'),
    sass = require('gulp-sass');

gulp.task('buildSass', function () {
    return gulp.src('styles.scss')
        .pipe(sass())
        .pipe(gulp.dest('build'));
});

gulp.task('buildSassCore', function () {
    return gulp.src('styles_core.scss')
        .pipe(sass())
        .pipe(gulp.dest('build'));
});

gulp.task('build', function (cb) {
    runSequence('buildSass', 'buildPage', cb);
});

gulp.task('initBuild', function (cb) {
    runSequence('buildSassCore', 'build', cb);
});

gulp.task('buildPage', function () {
    return gulp.src('index.html')
        .pipe(inlineCss({
            applyStyleTags: true,
            applyLinkTags: true,
            removeStyleTags: true,
            removeLinkTags: true,
            useCssAttr: true,
            removeHtmlSelectors: true
        }))
        .pipe(gulp.dest('build'))
        .pipe(livereload());
});

gulp.task('server', function (done) {
    http.createServer(
        st({path: __dirname + '/build', index: 'index.html', cache: false})
    ).listen(8080, done);
});

gulp.task('default', ['server', 'initBuild'], function () {
    livereload.listen({basePath: 'build'});
    gulp.watch('styles.scss', ['build']);
    gulp.watch('styles_core.scss', ['initBuild']);
    gulp.watch('index.html', ['buildPage']);
});


